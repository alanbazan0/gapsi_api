class AplicacionRepositorio extends Repositorio
{	
	constructor()
	{
		super("php/repositorios/AplicacionControlador.php");
	}
	
	consultarInformacion(contexto,funcion)
	{		
		
		var url = API + "/" + this.servicio;
		 $.ajax({
             url: url,
             type: 'POST',
             data: {accion : "consultarInformacion"},
             success: function( data, textStatus, jQxhr )
             {
                 funcion.call(contexto,data);
             },
             error: function( jqXhr, textStatus, errorThrown )
             {
            	 if(textStatus=="parsererror")
            		 funcion.call(contexto,{ mensajeError : jqXhr.responseText});
            	 else
            		 funcion.call(contexto,{ mensajeError : errorThrown});
             },
             fail: function( jqXhr, textStatus, errorThrown )
             {
            	 funcion.call(contexto,{ mensajeError : errorThrown});
             }
         });
		
	
	}
}