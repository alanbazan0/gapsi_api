class UsuariosRepositorio extends Repositorio
{	
	constructor()
	{
		super("php/repositorios/UsuariosControlador.php");
		$.ajaxSetup({
			  xhrFields: {
			    withCredentials: true
			  }
			});
	}
	

	
	iniciarSesion(contexto,funcion, nombreUsuario, contrasena, aplicacionId,aplicacionVersion)
	{		
		
		var url = API + "/" + this.servicio;
		 $.ajax({
             url: url,
             type: 'POST',
             data: {accion : "iniciarSesion", nombreUsuario : nombreUsuario, contrasena : contrasena, aplicacionId: aplicacionId,aplicacionVersion: aplicacionVersion },
             success: function( data, textStatus, jQxhr )
             {
                 funcion.call(contexto,data);
             },
             error: function( jqXhr, textStatus, errorThrown )
             {
            	 if(textStatus=="parsererror")
            		 funcion.call(contexto,{ mensajeError : jqXhr.responseText});
            	 else
            		 funcion.call(contexto,{ mensajeError : errorThrown});
             },
             fail: function( jqXhr, textStatus, errorThrown )
             {
            	 funcion.call(contexto,{ mensajeError : errorThrown});
             }
         });
		
	
	}
	
	cerrarSesion(contexto,funcion)
	{		
		var url = API + "/" + this.servicio;
		 $.ajax({
            url: url,
            type: 'POST',
            data: {accion : "cerrarSesion" },
            success: function( data, textStatus, jQxhr )
            {
                funcion.call(contexto,data);
            },
            error: function( jqXhr, textStatus, errorThrown )
            {
           	 funcion.call(contexto,{ mensajeError : errorThrown});
            },
            fail: function( jqXhr, textStatus, errorThrown )
            {
           	 funcion.call(contexto,{ mensajeError : errorThrown});
            }
        });
	}
	
	consultarSesion(contexto,funcion)
	{		
		var url = API + "/" + this.servicio;
		 $.ajax({
            url: url,
            type: 'POST',
            data: {accion : "consultarSesion" },
            success: function( data, textStatus, jQxhr )
            {
                funcion.call(contexto,data);
            },
            error: function( jqXhr, textStatus, errorThrown )
            {
           	 funcion.call(contexto,{ mensajeError : errorThrown});
            },
            fail: function( jqXhr, textStatus, errorThrown )
            {
           	 funcion.call(contexto,{ mensajeError : errorThrown});
            }
        });
	}
	
}