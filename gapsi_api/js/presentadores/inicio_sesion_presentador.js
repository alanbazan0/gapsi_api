class InicioSesionPresentador
{
	 constructor(vista)
	 {
		this.vista = vista; 
	 }
	 
	 iniciarSesion()
	 {
		 this.vista.mostrarIndicador();
		 var repositorio = new UsuariosRepositorio(this);		
		 repositorio.iniciarSesion(this,function(resultado)
		 {
			this.vista.ocultarIndicador();	
			if(resultado.mensajeError!=undefined)
			{
				if(resultado.mensajeError=="")
					this.vista.mostrarBienvenida(resultado.valor);
				else
					this.vista.mostrarMensajeError("Error",resultado.mensajeError);
			}
			else
				this.vista.mostrarMensajeError("Error",resultado);
		 },this.vista.nombreUsuario,this.vista.contrasena);
	 }
	 
	 
	 
	mostrarVersion()
	{
		 this.vista.mostrarIndicador();
		var repositorio = new AplicacionRepositorio(this);
		repositorio.consultarInformacion(this,function(resultado)
		{
			this.vista.ocultarIndicador();	
			if(resultado.mensajeError=="")
				this.vista.informacion = resultado.valor;
			else
				this.vista.mostrarMensajeError(resultado.mensajeError);
		});
	}
		
		
	 
	
	
	 
}