class TipoUsuario
{
	static get ADMINISTRADOR()
	{
		return 1;
	}
	
	static get COORDINADOR()
	{
		return  2;
	}
	
	static get USUARIO()
	{
		return 4;
	}
	
	static get INSPECTOR()
	{
		return  3;
	}
		
	static get SUPERVISOR()
	{
		return  5;
	}
	
	static get CAPACITADO()
	{
		return  6;
	}
	
}