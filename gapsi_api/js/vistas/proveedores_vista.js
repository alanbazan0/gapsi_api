//MVP Deging Pattern (view)
class ProveedoresVista extends CatalogoVista	
{		
	constructor(ventana)
	{	
		super(ventana);
		this.presentador = new ProveedoresPresentador(this);
		this._urlFormulario = "html/formularios/proveedores.php";
		
	}
	
	inicializar()
	{
		super.inicializar();
	}
	
	crearColumnasGrid()
	{
		this.tabla._columnas = [
			{longitud:50, 	titulo:"Id",   	alias:"id", alineacion:"D" },
			{longitud:200, 	titulo:"Nombre",   alias:"nombre", alineacion:"I", class:"desc" }, 
			{longitud:200, 	titulo:"Razón Social",   alias:"razonSocial", alineacion:"I" },		
			{longitud:200, 	titulo:"Dirección",   alias:"direccion", alineacion:"I" },		
			{longitud:250, 	titulo:"Fecha de alta",   alias:"fechaAlta", alineacion:"I" },	
			{longitud:200, 	titulo:"Fecha de última modificación",   alias:"fechaModificacion", alineacion:"I" },
			{longitud:100, 	titulo:"Estatus",   alias:"estatus", alineacion:"D", itemRenderer:this.renderEstatus}
		]
		
		this.tabla.contenidoAdicional = "<button data-toggle='tooltip' data-placemen='bottom' title='Editar'  type='button' class='editar btn-circle mr-0 botones-icon btn btn-sm float-left btn-info active'><span  data-toggle='tooltip' class='fa fa-edit fa-lg'></span></button>"+
									"<button data-toggle='tooltip' data-placemen='bottom' title='Eliminar'  type='button' class='eliminar btn-circle mr-0 botones-icon btn btn-sm float-left btn-danger active'><span  data-toggle='tooltip' class='fa fa-minus-circle fa-lg'></span></button>";

		this.tabla.registros = [];
	}
	
	inicializarValidacionesFormulario()
	{
		var _this = this;
		jQuery("#formulario").validate({
            ignore: [],
            errorClass: "invalid-feedback animated fadeInDown",
            errorElement: "div",
            errorPlacement: function(e, a) {
                jQuery(a).parents(".form-group > div").append(e)
            },
            highlight: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
            },
            success: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
            },
            rules: {
            	 "nombreInput": {required: !0},
                "razonSocialInput": {required: !0},
                "direccionInput": {required: !0}
               
            },
            messages: {
            	 "nombreInput": "Por favor ingrese una nombre",
            	 "razonSocialInput": "Por favor ingrese una razón social",
                "direccionInput": "Por favor ingrese una dirección"
                	
                
            },
            submitHandler:function (form) {
            	 _this.guardar();
            }
        });
	}
	
	editar(id)
	{
		super.editar(id);
		$('#nombreInput').focus();
	}
	
	get criteriosSeleccion()
	{
		 var criteriosSeleccion = 
		 {				    
			nombre: $('#nombreInputCriterio').val(),
			razonSocial: $('#razonSocialInputCriterio').val(),
			direccion:$('#direccionInputCriterio').val()
		 }
		 return criteriosSeleccion;
	}		
	
	set modelo(valor)
	{		
		this.modeloEdicion = valor;
		$('#nombreInput').val(this.modeloEdicion.nombre);
		$('#razonSocialInput').val(this.modeloEdicion.razonSocial);
		$('#direccionInput').val(this.modeloEdicion.direccion);
		if(this.modeloEdicion.estatus==1)
			$("#estatusRadio").prop('checked', true);
		else
			$("#estatusRadio").prop('checked', false);
	}
	
	get modelo()
	{
		 var modelo = 
		 {		
			nombre:$('#nombreInput').val(),
			razonSocial:$('#razonSocialInput').val(),
			direccion:$('#direccionInput').val(),
			 estatus:$('#estatusRadio').is(':checked')?1:0
		 };
	
		 if(this.modo==Modo.CAMBIO && this.modeloEdicion!=null)
			 modelo.id = this.modeloEdicion.id;
		 return modelo;
	 }
	 

	limpiarFormulario()
	{
		$('#nombreInput').val("");
		$('#razonSocialInput').val("");
		$('#direccionInput').val("");
	}
	
}
var vista = new ProveedoresVista(this);
$(document).ready(function() 
{
	vista.inicializar();
});
