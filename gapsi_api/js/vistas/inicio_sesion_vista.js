class InicioSesionVista extends Vista
{
	constructor(ventana)
	{	
		super();
		this.ventana = ventana;
		this.presentador = new InicioSesionPresentador(this);
	}
	
	inicializar()
	{
		this.renderizarFondo();
		this.mostrarVersion();
		this.inicializarValidacionesCuenta();
	}
	
	
	recuperarCuenta()
	{
		this.presentador.recuperarCuenta();
	}
	
	get url()
	{
		return $("body").attr("data-url");
	}
	
	
	
	inicializarValidacionesCuenta()
	{
        jQuery("#inicioSesionForm").validate({
            ignore: [],
            errorClass: "invalid-feedback animated fadeInDown",
            errorElement: "div",
            errorPlacement: function(e, a) {
                jQuery(a).parents(".form-group > div").append(e)
            },
            highlight: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
            },
            success: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
            },
            rules: {
                "nombreUsuarioInput": {
                    required: !0
                },
                "contrasenaInput": {
                    required: !0
                }
            },
            messages: {
                "nombreUsuarioInput": "Por favor ingrese una direcci\xF3n de correo electr\xF3nico v\xE1lida",
                "contrasenaInput": {
                    required: "Por favor ingrese una contrase\xF1a"
                }
            },
            submitHandler:function (form) {
            	 vista.iniciarSesion();
            }
        });
	}
	
	mostrarVersion()
	{
		this.presentador.mostrarVersion();
	}
	
	iniciarSesion()
	{
		this.presentador.iniciarSesion();
	}
	

	get nombreUsuario()
	{
		return $("#nombreUsuarioInput").val();
	}
	
	get contrasena()
	{
		return $("#contrasenaInput").val();
	}
	
	
	mostrarBienvenida(usuario)
	{
		var _this = this;
		swal({
	            title: "Bienvenido",
	            text: "<strong>"+usuario.nombreCompleto+"</strong>",
	            type: "success",
				html:true,
	            //confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Continuar",
	            closeOnConfirm: true,
	            closeOnCancel: true,
	            showLoaderOnConfirm: true,
	        },
	        function(isConfirm)
	        {
	            if (isConfirm) 
	            {
	            	 setTimeout(function(){
	            		var url = "index.php"
						if(this.url!="" && this.url!=undefined)
						{
							url = this.url;
						}
						
						var submitForm = _this.getNewSubmitForm(url);
						_this.createNewFormElement(submitForm, "usuario", JSON.stringify(usuario));	 
					    submitForm.target= "_self";
					    submitForm.submit();
	 	            }, 1000);
	            }
	        });

		
		
	}
	
	
	consultarFrases()
	{
		 this.vista.mostrarIndicador();
		 var repositorio = new FrasesRepositorio(this);		
		 repositorio.consultar(this, function(resultado)
		 {
				vista.ocultarIndicador();	
				if(resultado.mensajeError=="")
			    {
					var frases = resultado.valor;
					if(frases.length>0)
					{
						var indice = Math.floor((Math.random() * frases.length));
						var frase = frases[indice];
						$("#fraseP").html(frase.texto);
				 		$("#autorP").html(frase.autor);
						
					}
			    }
				else
					this.vista.mostrarMensajeError("Error",resultado.mensajeError);
				
		 });
	}
	

  	renderizarFondo()
	{
	  fetch('https://source.unsplash.com/random/1600x900').then((response) => {   
	  $("body").css("background-image","url("+response.url+")");
	  }) 
	}
	
	set informacion(informacion)
	{
		$("#versionSpan").html(informacion.nombre + " "+  informacion.version);
	}
}
var vista = new InicioSesionVista();
$(document).ready(function() 
{
	vista.inicializar();
});