<?php
namespace php\interfaces;

use php\modelos\Proveedor;

interface IProveedoresRepositorio
{
    public function insertar(Proveedor $modelo);
    public function actualizar(Proveedor $modelo);  
    public function consultar($criteriosSeleccion);  
    public function consultarPorLlaves($llaves); 
}

