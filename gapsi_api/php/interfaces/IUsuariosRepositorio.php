<?php
namespace php\interfaces;

use php\modelos\Usuario;

interface IUsuariosRepositorio
{
    public function consultarUsuario($nombreUsuario, $contraseña);    
}

