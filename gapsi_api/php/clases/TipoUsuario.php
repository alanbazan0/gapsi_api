<?php
abstract class TipoUsuario
{
    const ADMINISTRADOR = 1;
    const SUPERVISOR = 5;
    const COORDINADOR = 2;
    const USUARIO = 4;
    const INSPECTOR = 3;
    const CAPACITADO = 6;
}