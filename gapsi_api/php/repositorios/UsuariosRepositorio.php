<?php
namespace php\repositorios;



use php\interfaces\IUsuariosRepositorio;
use php\modelos\Usuario;
use php\modelos\Resultado;

require_once("../interfaces/IUsuariosRepositorio.php");


require_once("RepositorioBase.php");
require_once("../clases/Resultado.php");
require_once("../vendor/simplexlsx/src/SimpleXLSX.php");

class UsuariosRepositorio extends RepositorioBase implements IUsuariosRepositorio
{
    protected $conexion;
    protected $consultaBase;
    public function __construct($conexion)
    {
        $this->conexion = $conexion;
        $this->consultaBase = "SELECT U.id, U.nombre_usuario, U.contrasena ,U.nombre, U.apellido, IFNULL(DATE_FORMAT(U.fecha_alta,'%d/%m/%Y %H:%i:%s'),'') fecha_alta,  IFNULL(DATE_FORMAT(U.fecha_modificacion,'%d/%m/%Y %H:%i:%s'),'')fecha_modificacion,U.estatus, tipo_usuario_id, TU.nombre 
                             FROM usuarios U
                                   INNER JOIN tipos_usuario TU ON U.tipo_usuario_id = TU.id";
       
    }    
    
  
    private function crearRegistro($id, $nombreUsuario, $contrasena, $nombre, $apellido, $fechaAlta, $fechaModificacion, $estatus, $tipoUsuarioId, $tipoUsuarioNombre)
    {
        $registro= (object) [
            'id' =>  $id,
            'nombreUsuario' => $nombreUsuario,
            'contrasena' => $contrasena,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'fechaAlta' => $fechaAlta,
            'fechaModificacion' => $fechaModificacion,
            'estatus' => $estatus,
            'tipoUsuarioId' => $tipoUsuarioId,
            'tipoUsuarioNombre' => $tipoUsuarioNombre
        ];
       
        
        $registro->nombreCompleto = $registro->nombre . " " . $registro->apellido;
        $registro->fotoPerfil =  "../fotos/usuario". $registro->id .".jpg";
        if(file_exists($registro->fotoPerfil))
            $registro->fotoPerfil =  "php/fotos/usuario". $registro->id .".jpg";
        else
             $registro->fotoPerfil =  "php/fotos/default.jpg";
        
        
        return $registro;
    }
    
   
    

    public function consultarUsuario($nombreUsuario,$contrasenaUsuario)
    {
        $resultado = new Resultado();       
        $consulta =   $this->consultaBase .
                    " WHERE U.nombre_usuario = ?";
        
        
        if($sentencia = $this->conexion->prepare($consulta))
        {
            if($sentencia->bind_param("s",$nombreUsuario))
            {
                if($sentencia->execute())
                {                    
                    if ($sentencia->bind_result($id, $nombreUsuario, $contrasena, $nombre, $apellido, $fechaAlta, $fechaModificacion, $estatus, $tipoUsuarioId,$tipoUsuarioNombre)  )
                    {
                        if($sentencia->fetch())
                        {
                            $registro = $this->crearRegistro($id, $nombreUsuario, $contrasena, $nombre, $apellido, $fechaAlta, $fechaModificacion, $estatus, $tipoUsuarioId,$tipoUsuarioNombre);
                            //TODO: usar alguna tecnica de encriptacion
                            if(trim($registro->contrasena)==trim($contrasenaUsuario))
                                $resultado->valor = $registro;
                            else 
                                $resultado->mensajeError = "La combinación de usuario y contraseña es incorrecta.";
                        }
                        else
                            $resultado->mensajeError = "El usuario $nombreUsuario no existe";
                    }
                    else
                        $resultado->mensajeError = "Falló el enlace del resultado";
                }
                else
                    $resultado->mensajeError = "Falló la ejecución (" . $this->conexion->errno . ") " . $this->conexion->error;  
            }
            else
                $resultado->mensajeError = "Falló el enlace de parámetros";    
        } 
        else
            $resultado->mensajeError = "Falló la preparación: (" . $this->conexion->errno . ") " . $this->conexion->error;     
       return $resultado;
    }
    
}
?>