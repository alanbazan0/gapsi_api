<?php
use php\clases\AdministradorConexion;
use php\clases\JsonMapper;
use php\modelos\Usuario;

use php\repositorios\UsuariosRepositorio;
use php\modelos\Resultado;

error_reporting(E_ALL);
ini_set('display_errors', 1);


include '../clases/JsonMapper.php';
include '../clases/Utilidades.php';
include '../configuracion.php';
include '../clases/AdministradorConexion.php';
include '../modelos/Usuario.php';
include '../clases/TipoUsuario.php';
include '../repositorios/UsuariosRepositorio.php';




$origin = "*";
if(isset($_SERVER['HTTP_ORIGIN']))
    $origin =$_SERVER['HTTP_ORIGIN'];
header('Access-Control-Allow-Origin: '.$origin);
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Credentials: true');


// if($session_cookie_domain!="")
//     ini_set('session.cookie_domain', $session_cookie_domain);

$administrador_conexion = new AdministradorConexion();
$resultado = new Resultado();
$conexion=null;
try
{
    $conexion = $administrador_conexion->abrir();
    if($conexion)
    {
        $accion = REQUEST('accion');
        $repositorio = new UsuariosRepositorio($conexion);
        switch ($accion)
        {           
            case 'iniciarSesion':
                session_start();
                $nombreUsuario = REQUEST('nombreUsuario');
                $contrasena = REQUEST('contrasena');
                $resultado = $repositorio->consultarUsuario($nombreUsuario,$contrasena);
                if($resultado->valor!=null)
                {
                    $resultado->valor->contrasena = "*****";
                    $validarSeguridad = true;
                    if($validarSeguridad)
                    {
                        $tienePermiso = false;
                        
                        if($resultado->valor->estatus==1)
                            $tienePermiso = true;
                       
                        if($tienePermiso)
                        {
                            $_SESSION['usuario']=$resultado->valor;
                        }
                        else 
                        {
                            $resultado->valor = null;
                            $resultado->mensajeError="El acceso a la plataforma en linea esta restringido a usuarios autorizados, si necesita ingresar para realizar cambios por favor solicite los cambios con su supervisor autorizado.";
                            unset($_SESSION['usuario']);
                            
                        }
                    }
                    else 
                    {
                        $_SESSION['usuario']=$resultado->valor;
                        //TODO: Guardar historial de acceso, aplicacion, version, ip, user-agent
                        //$historialAccesoRepositorio = new HistorialAccesoRepositorio($conexion);
                        //$historialAccesoRepositorio->insertar($nombreUsuario,$aplicacionId,$aplicacionVersion);
                    }
                    
                }
                else
                {
                    unset($_SESSION['usuario']);
                    
                }
            break;
            case "cerrarSesion":
                session_start();
                if(isset($_SESSION['usuario']))
                {
                    unset($_SESSION['usuario']);
                }
                $resultado->valor = true;
            break;
            case "consultarSesion":
                session_start();
                if(isset($_SESSION['usuario']))
                    $resultado->valor = $_SESSION['usuario'];
                    
            break;
            default:
                $resultado->mensajeError = 'Acción no implementada';
            break;
            
            
            
        }
    }
    
}
catch(Exception $e)
{   
    $resultado->mensajeError = $e->getMessage();
}
finally
{
    if($resultado!=null)
    {
        $json = json_encode($resultado, JSON_PRETTY_PRINT);
        if (FALSE === $json)
            echo '{"mensajeError":"' .json_last_error_msg() . '"}';
            else
                echo $json;
    }
    $administrador_conexion->cerrar($conexion);
}


