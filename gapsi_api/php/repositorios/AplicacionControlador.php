<?php
use php\clases\AdministradorConexion;
use php\clases\JsonMapper;
use php\modelos\Usuario;

use php\repositorios\UsuariosRepositorio;
use php\modelos\Resultado;

error_reporting(E_ALL);
ini_set('display_errors', 1);


include '../clases/JsonMapper.php';
include '../clases/Utilidades.php';
include '../configuracion.php';
include '../clases/AdministradorConexion.php';
include '../modelos/Usuario.php';
include '../clases/TipoUsuario.php';
include '../clases/Resultado.php';
//include '../repositorios/AplicacionRepositorio.php';




$origin = "*";
if(isset($_SERVER['HTTP_ORIGIN']))
    $origin =$_SERVER['HTTP_ORIGIN'];
header('Access-Control-Allow-Origin: '.$origin);
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Credentials: true');


// if($session_cookie_domain!="")
//     ini_set('session.cookie_domain', $session_cookie_domain);

$administrador_conexion = new AdministradorConexion();
$resultado = new Resultado();
$conexion=null;
try
{
    $conexion = $administrador_conexion->abrir();
    if($conexion)
    {
        $accion = REQUEST('accion');
        switch ($accion)
        {           
            case "consultarInformacion":
                //TODO: cargar desde archivo json
                $resultado->valor = (object)["nombre"=>"Gapsi", "version" => "1.0.0"];
                    
            break;
            default:
                $resultado->mensajeError = 'Acción no implementada';
            break;
            
            
            
        }
    }
    
}
catch(Exception $e)
{   
    $resultado->mensajeError = $e->getMessage();
}
finally
{
    if($resultado!=null)
    {
        $json = json_encode($resultado, JSON_PRETTY_PRINT);
        if (FALSE === $json)
            echo '{"mensajeError":"' .json_last_error_msg() . '"}';
            else
                echo $json;
    }
    $administrador_conexion->cerrar($conexion);
}


